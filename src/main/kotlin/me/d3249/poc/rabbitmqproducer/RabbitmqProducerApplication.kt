package me.d3249.poc.rabbitmqproducer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import kotlin.concurrent.thread


@SpringBootApplication
class RabbitmqProducerApplication

fun main(args: Array<String>) {
    runApplication<RabbitmqProducerApplication>(*args)

}
