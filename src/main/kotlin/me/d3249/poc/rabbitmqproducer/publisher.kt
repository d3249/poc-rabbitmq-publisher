package me.d3249.poc.rabbitmqproducer

import org.springframework.amqp.core.AmqpTemplate
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@EnableScheduling
@Component
class RabbitMQProducer(private val amqpTemplate: AmqpTemplate) {

    @Scheduled(fixedRate = 5000L)
    fun sendTime() = amqpTemplate.convertAndSend(LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE))
}

